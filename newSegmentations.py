import numpy as np


def segment_img_to_grid(img_array, tiles_split=(5,5)):
    tiles_per_col, tiles_per_row = tiles_split
    mask = np.zeros([img_array.shape[0], img_array.shape[1]])

    tile_row_size = mask.shape[0]/tiles_per_col
    tile_col_size = mask.shape[1]/tiles_per_row

    for row_seg in range(tiles_per_col):
        for col_seg in range(tiles_per_row):
            col_start = tile_col_size * col_seg
            if mask.shape[1] - (col_start + tile_col_size) > tile_col_size:
                col_end = col_start + tile_col_size
            else:
                col_end = mask.shape[1]
            row_start = tile_row_size * row_seg
            if mask.shape[0] - (row_start + tile_row_size) > tile_row_size:
                row_end = row_start + tile_row_size
            else:
                row_end = mask.shape[0]

            for col in range(col_start,col_end):
                for row in range(row_start, row_end):
                    mask[row][col] = (tiles_per_row * row_seg)+(1+col_seg)
                    last_seg = (tiles_per_row * row_seg)+(1+col_seg)

    return mask
